
var promise = Promise.resolve(1); // vs. Promise.reject(new Error('failed'));

promise.then(result => {
    console.log('1: ' + result);

    return getData(result);
}).then(result => {
    console.log('2: ' + result);

    return 1;
}).then(result => {
    // no return
}).then(result => {
    throw new Error('sth bad happened');
}).then(result => {
    // skipped
}).catch(error => {
    // process error
    // throw error
    // return 1
}).then(result => {
    // executed if catch
    // does not throw
});

function getData() {
    return Promise.resolve('some data');
    // return Promise.reject(new Error('no data'));
}
