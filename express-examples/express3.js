'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.json());

app.get('/api/contacts', getContacts);
app.post('/api/contacts', saveContact);

app.listen(3000);

function getContacts(request, response) {
    response.json([{ id: 1 }, { id: 2 }]);
}

function saveContact(request, response) {
    console.log(request.body); // request should have correct content-type
    response.end();
}
