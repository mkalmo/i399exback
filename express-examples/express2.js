'use strict';

const express = require('express');
const app = express();

app.use(express.static('./'));

app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);

app.listen(3000);

function getContacts(request, response) {
    response.set('Content-Type', 'application/json');
    response.end(JSON.stringify([{ id: 1 }, { id: 2 }]));
}

function getContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    response.end(JSON.stringify({ id: id }));
}
